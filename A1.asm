.data

array: 	.word 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
str: 	.asciiz "Enter the number of elements: "
str1: 	.asciiz "The number must be between (1-20).\n"
str2: 	.asciiz "The number must be positive.\n"
str3: 	.asciiz "The number must be divisible by 3.\n"
str4: 	.asciiz "\n"
str5: 	.asciiz "The array contents are:\n"
str6: 	.asciiz "The reversed array contents are:\n"
str7: 	.asciiz "I'm here\n"

.text
main: 	jal readNum
	add $a1, $v1, $0
	add $s0, $v1, $0
	jal verifySize
	add $t0, $v0, $0
	add $t1, $v1, $0
	li $t2, 1
	beq $t0, $t2, error
	beq $t1, $t2, error
	add $a1, $s0, $0
	jal createArray
	add $a1, $s0, $0
	li $v0, 4
	la $a0, str5
	syscall
	jal printArray
	add $a1, $s0, $0
	jal reverseArray
	add $a1, $s0, $0
	li $v0, 4
	la $a0, str6
	syscall
	jal printArray
	li $v0, 10
	syscall

readNum:
	li $v0, 4
	la $a0, str
	syscall
	li $v0, 5
	syscall
	add $v1, $v0, $0
	jr $ra

verifySize: 
	add $s1, $a1, $0
	li $t0, 1
	li $t1, 20
	slt $v0, $s1, $t0
	sgt $v1, $s1, $t1
	jr $ra


createArray: 
	add $s1, $a1, $0
	addi $sp, $sp, -4
	sw $ra, 0($sp)
	li $t0, 0 #counter
	la $s2, array
makingArray: 
	beq $t0, $s1, next
	jal readNum
	add $t1, $v1, $0
	add $a1, $t1, $0
	jal checkPos
	add $t2, $v0, $0
	beq $t2, $0, error1
	add $a1, $t1, $0
	jal divBy3
	add $t2, $v1, $0
	beq $t2, $0, error2
	sw $t1, 0($s2)
	addi $s2, $s2, 4
	addi $t0, $t0, 1
	j makingArray


next: 	lw $ra, 0 ($sp)
	addi $sp, $sp, 4
	jr $ra #save address in stack

checkPos: 
	add $t2, $a1, $0
	sgt $v0, $t2, $0
	jr $ra

divBy3: add $t3, $a1, $0
	li $t4, 3
	div $t3, $t4
	mfhi $t3
	seq $v1, $t3, $0
	jr $ra


printArray: 
	add $s1, $a1, $0
	la $s2, array
	li $t0, 0
printLoop: 
	beq $t0, $s1, done
	li $v0, 1
	lw $a0, 0($s2)
	syscall
	li $v0, 4
	la $a0, str4
	syscall
	addi $s2, $s2, 4
	addi $t0, $t0, 1
	j printLoop

done: 	jr $ra

reverseArray: 
	add $s1, $a1, $0
	li $t0, 4
	mult $s1, $t0
	mflo $t1
	li $t0, 2
	div $s1, $t0
	mflo $t0
	la $s1, array
	la $s2, array
	add $s2, $s2, $t1
	addi $s2, $s2, -4
reverseLoop: 
	beq $t0, $0, done
	lw $t1, 0($s2)
	lw $t2, 0($s1)
	sw $t2, 0($s2)
	sw $t1, 0($s1)
	addi $s1, $s1, 4
	addi $s2, $s2,-4
	addi $t0, $t0, -1
	j reverseLoop

error: 	li $v0, 4
	la $a0, str1
	syscall
	j main

error1: li $v0, 4
	la $a0, str2
	syscall
	j makingArray

error2: li $v0, 4
	la $a0, str3
	syscall
	j makingArray